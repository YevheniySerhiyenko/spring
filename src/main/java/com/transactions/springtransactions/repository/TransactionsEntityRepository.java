package com.transactions.springtransactions.repository;

import com.transactions.springtransactions.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionsEntityRepository extends JpaRepository<TransactionEntity, Long> {
}
