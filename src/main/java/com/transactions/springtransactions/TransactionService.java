package com.transactions.springtransactions;

import com.transactions.springtransactions.entity.ProxyType;
import com.transactions.springtransactions.entity.TransactionEntity;
import com.transactions.springtransactions.repository.TransactionsEntityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class TransactionService implements ITransactionService {

    private final TransactionsEntityRepository repository;

    //    @Transactional
    public void publicTransactionMethodSuccess(ProxyType proxyType) {
        repository.save(new TransactionEntity(proxyType));
        log.info("publicTransactionMethod");
    }

    //    @Transactional
    public void publicTransactionMethodFailed(ProxyType proxyType) {
        repository.save(new TransactionEntity(proxyType));
        log.info("publicTransactionMethod");
        throw new RuntimeException();
    }

    @Transactional
    protected void protectedTransactionMethodSuccess(ProxyType proxyType) {
        repository.save(new TransactionEntity(proxyType));
        log.info("protectedTransactionMethod");
    }

    @Transactional
    protected void protectedTransactionMethodFailed(ProxyType proxyType) {
        repository.save(new TransactionEntity(proxyType));
        log.info("protectedTransactionMethod");
        throw new RuntimeException();
    }

    @Transactional
    void packagePrivateTransactionMethodSuccess(ProxyType proxyType) {
        repository.save(new TransactionEntity(proxyType));
        log.info("packagePrivateTransactionMethod");
    }

    @Transactional
    void packagePrivateTransactionMethodFailed(ProxyType proxyType) {
        repository.save(new TransactionEntity(proxyType));
        log.info("packagePrivateTransactionMethod");
        throw new RuntimeException();
    }


    //does not work
    private void privateTransactionMethod(ProxyType proxyType) {
        log.info("privateTransactionMethod");
    }

}
