package com.transactions.springtransactions;

import com.transactions.springtransactions.entity.ProxyType;
import lombok.RequiredArgsConstructor;
import org.springframework.aop.support.AopUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;


// AbstractAutowireCapableBeanFactory 416 line
// TransactionInterceptor invoke 123 line
// DefaultAopProxyFactory
@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass = false)
@RequiredArgsConstructor
public class SpringTransactionsApplication implements CommandLineRunner {

	private final TransactionService transactionService;

	public static void main(String[] args) {
		SpringApplication.run(SpringTransactionsApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		ProxyType proxyType = getProxyType(transactionService);
		transactionService.protectedTransactionMethodFailed(proxyType);
	}

	private ProxyType getProxyType(Object object) {
		boolean cglibProxy = AopUtils.isCglibProxy(object);
		boolean jdkDynamicProxy = AopUtils.isJdkDynamicProxy(object);
		boolean aopProxy = AopUtils.isAopProxy(object);
		ProxyType proxyType = ProxyType.NULL;
		if (aopProxy) {
			proxyType = ProxyType.AOP;
		}
		if (cglibProxy) {
			proxyType = ProxyType.CGLIB;
		}
		if (jdkDynamicProxy) {
			proxyType = ProxyType.JDK;
		}
		return proxyType;
	}
}
