package com.transactions.springtransactions;

import com.transactions.springtransactions.entity.ProxyType;

public interface ITransactionService {
    void publicTransactionMethodSuccess(ProxyType proxyType);

    void publicTransactionMethodFailed(ProxyType proxyType);
}
