package com.transactions.springtransactions.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.time.ZonedDateTime;

@Getter
@Setter
@Entity
@Table(name = "transactions")
@RequiredArgsConstructor
public class TransactionEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private ZonedDateTime createdAt;

    @Enumerated(EnumType.STRING)
    private ProxyType proxyType;


    public TransactionEntity(ProxyType proxyType) {
        this.description = "description " + ZonedDateTime.now();
        this.createdAt = ZonedDateTime.now();
        this.proxyType = proxyType;
    }
}
