package com.transactions.springtransactions.entity;

public enum ProxyType {
    AOP,
    CGLIB,
    JDK,
    NULL
}
